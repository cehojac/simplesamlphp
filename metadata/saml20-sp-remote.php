<?php
/**
 * SAML 2.0 remote SP metadata for SimpleSAMLphp.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-sp-remote
 */

$metadata['http://fs.pepper.com.au/adfs/services/trust'] = array (
  'entityid' => 'http://fs.pepper.com.au/adfs/services/trust',
  'description' =>
  array (
    'en-AU' => 'Pepper Group',
  ),
  'OrganizationName' =>
  array (
    'en-AU' => 'Pepper Group',
  ),
  'name' =>
  array (
    'en-AU' => 'Pepper Group',
  ),
  'OrganizationDisplayName' =>
  array (
    'en-AU' => 'Pepper Group',
  ),
  'url' =>
  array (
    'en-AU' => 'https://www.peppergroup.com.au/',
  ),
  'OrganizationURL' =>
  array (
    'en-AU' => 'https://www.peppergroup.com.au/',
  ),
  'contacts' =>
  array (
    0 =>
    array (
      'contactType' => 'support',
      'givenName' => 'Sean',
      'surName' => 'Hegyi',
      'emailAddress' =>
      array (
        0 => 'dlbtsinfrastructureservices@pepper.com.au',
      ),
      'telephoneNumber' =>
      array (
        0 => '+61 2 8913 3030',
      ),
    ),
  ),
  'metadata-set' => 'saml20-sp-remote',
  'AssertionConsumerService' =>
  array (
    0 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://fs.pepper.com.au/adfs/ls/',
      'index' => 0,
      'isDefault' => true,
    ),
    1 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
      'Location' => 'https://fs.pepper.com.au/adfs/ls/',
      'index' => 1,
    ),
    2 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://fs.pepper.com.au/adfs/ls/',
      'index' => 2,
    ),
  ),
  'SingleLogoutService' =>
  array (
    0 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://fs.pepper.com.au/adfs/ls/',
    ),
    1 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://fs.pepper.com.au/adfs/ls/',
    ),
  ),
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
  'keys' =>
  array (
    0 =>
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIC4jCCAcqgAwIBAgIQNFjwOuakAahFJDIJPE7J8DANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJBREZTIEVuY3J5cHRpb24gLSBmcy5wZXBwZXIuY29tLmF1MB4XDTE2MDUwNzEyMTU0MloXDTI2MDUwNTEyMTU0MlowLTErMCkGA1UEAxMiQURGUyBFbmNyeXB0aW9uIC0gZnMucGVwcGVyLmNvbS5hdTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJfSQ5uMMQedzkR81+YZa8fFaw0lfh66oXSHrQOQgZ2O+cDHGBHmdeK8SPBhpMQvrJ30C2rvc2PEhrNkes7tPknUMq/j0DOYjHPeqcEOjC628CFJxEnHWfXMVAGvge9gzIEOjYoJKR2u4hCvsMkfSzvzqTfBRXK+ZMu0+LLspWcM55RjP9pbJUiw1zxBHDnPCROC8dnW77H+SIC49dRxwU2nnH45pEhftGOvmlRRJ+GFyhA9weZ3Xg+2EKxcGpwl2IRMFrIvioi21QIRZg2X3jAX0NZXswDy7BHKS2ACCi+W9nGP/dLACZ2fOGgR2gpaKbVK7vrjehaodyLD9lD/gcsCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAfIV1p+YZpJTDo71GlZO6zWIK+/IeMFLljYCOeWKrbvWNHb2Jg+XQaWzzNfLu6xcKo+ZqS8NvdTBSOnccNbG1VqgpI7DtKfGT7TmxmqSPy5ufwELVN8MvlNVtCLlDyYnAbQSAROcYZ045i5ak9fkzZsrg0aF3nKHJ9ByrCtqN5kaR77G54i0Cbwe5zuyXMItPd2FAP4WtzHeU8uHAVTR4Ef/zZ0wSTjaiGG6KouA90T1wC1Bxn/qEZ1B12SsAfREy6nCL0CL6u0dxIMX8CaxhsQ93banSPnw05Pa0QshbjX8YTwNH4dZpmk4AKeok2RN/PKspk8+mX4Nr4iEPYaWlNg==',
    ),
    1 =>
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIC3DCCAcSgAwIBAgIQb9ItZBdemYNH6HOyjt6pcjANBgkqhkiG9w0BAQsFADAqMSgwJgYDVQQDEx9BREZTIFNpZ25pbmcgLSBmcy5wZXBwZXIuY29tLmF1MB4XDTE2MDUwNzEyMTU0M1oXDTI2MDUwNTEyMTU0M1owKjEoMCYGA1UEAxMfQURGUyBTaWduaW5nIC0gZnMucGVwcGVyLmNvbS5hdTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM5zUeqaXhuoUATkfBSGvSKbRBLmzG2QqKGsuCQk3X6JJbVpqP2NU2ysW9jSw0e1TLOhdAnOjnmB3Bt5HCh2QqlfAblx4ADTeROQ+nT2CEu95LLoqvDyyYxNzVH58F9WXYZb4wfWZ3g4e/GDsEMT5qhhDux1ktvl2qlDDydoP9kHsiikDBa+7jZ+mSUude+zs2hmPjNFJeWmGqYNL4Ndoc73LZlE4E4rwWfeF7Y5I4btOVY8jLltrDRDv+Z9zdNeJ38C5SWVFAVBy6+2l3V3/aW7cwjARk69agYYGM/8kro5ISkAFqVBfCTEg4kBH1uM/BODJiiE4yQgmiu0dyQzogcCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAE+/vavZVlE/hOeLLf673LrrCGgUfcg607zeKjidoAGzQt07BP43WnL7UQN623ddiI+HYhYWiPuUjdCVlOyKDreiTkqbceTeYJcb6dzU9d4/xgK6wlaU80dqbdikc3Q5QucmFXiTAV8Y3yjlQuUOMpUSVB20MouX2c+YV3oXu4JMf37b5IhrTvderGylvOUTDBQFTpSmBscAx5RvjvFhichZMWQTBnFEFnCZsFIlGP4bI4kkDAWCVqW+2i8W38dEZnI10nWOEy2wMV6/Gi3UOpKWyERT++qw3T262uE2qrT84kYsaAhiqS2nCdxfM7UbchuJza1CEyYh4XFcpF2NTTw==',
    ),
  ),
  'saml20.sign.assertion' => true,
);

